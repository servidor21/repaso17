
@extends('layouts.app')

@section('content')
<div class="container">


<h1>
    Vista de estudiantes
</h1>

<a href="/students/create">
Alta de estudiante
</a>

<table class="table">   

    <tr>
        <th>Id</th>
        <th>DNI</th>
        <th>Nombre</th>
        <th>Apellido</th>
        <th></th>
    </tr>


@foreach ($students as $student)
    <tr>
        <td>{{ $student->id }}</td>
        <td>{{ $student->dni }}</td>
        <td>{{ $student->firstname }}</td>
        <td>{{ $student->lastname }}</td>
        <td>
        <a href="/students/{{ $student->id }}">Ver</a>
        <a href="/students/{{ $student->id }}/edit">Actualizar</a>

        <form action="/students/{{ $student->id }}" method="post">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
        <input type="submit" value="borrar">
        </form>


        </td>

    </tr>
@endforeach
</table>
{{ $students->links() }}
</div>
@endsection
