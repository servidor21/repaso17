@extends('layouts.app')

@section('content')
<div class="container">


<h1>
    Alta de estudiantes
</h1>

<ul>
    <li>DNI: {{ $student->dni }}</li>
    <li>Nombre: {{ $student->firstname }}</li>
    <li>Apellido: {{ $student->lastname }}</li>
</ul>

<p><a href="/students/{{ $student->id }}/edit">Modificar</a></p>
</div>
@endsection
