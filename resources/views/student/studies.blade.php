@extends('layouts.app')

@section('content')
<div class="container">


<h1>
    Alta de estudios
</h1>

<ul>
    <li>DNI: {{ $student->code }}</li>
    <li>Nombre: {{ $student->firstname }}</li>
    <li>Apellido: {{ $student->lastname }}</li>
</ul>

<p><a href="/students/{{ $student->id }}/edit">Modificar</a></p>

    <h3>Estudios del estudiante actual</h3>
    <ol>
        @foreach ($student->studies as $study)
        <li>{{ $study->code }} - {{ $study->name }} - {{ $study->shortName }} - {{ $study->abreviation }}Curso  {{ $study->pivot->course }}º - Año {{ $study->pivot->year }}</li>
        @endforeach
    </ol>
</div>
@endsection
