@extends('layouts.app')

@section('content')
<div class="container">


<h1>
    Alta de estudiantes
</h1>

<div class="form">
<form action="/students/{{ $student->id }}" method="post">
    {{ csrf_field() }}
    {{ method_field('PUT') }}


    <div class="form-group">
        <label>DNI: </label>
        <input type="text" name="dni" value="{{ $student->dni }}">
    </div>

    <div class="form-group">
        <label>Nombre: </label>
        <input type="text" name="firstname" value="{{ $student->firstname }}">
    </div>

    <div class="form-group">
        <label>Apellido: </label>
        <input type="text" name="lastname" value="{{ $student->lastname }}">
    </div>

    <div class="form-group">
        <input type="submit" value="Guardar">
    </div>    
</form>
</div>
</div>
@endsection
