@extends('layouts.app')

@section('content')
<div class="container">


<h1>
    Alta de familias
</h1>

<ul>
    <li>Codigo: {{ $family->code }}</li>
    <li>Nombre: {{ $family->name }}</li>
</ul>

<p><a href="/families/{{ $family->id }}/edit">Modificar</a></p>
</div>
@endsection
