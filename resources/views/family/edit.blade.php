@extends('layouts.app')

@section('content')
<div class="container">


<h1>
    Alta de familias
</h1>

<div class="form">
<form action="/families/{{ $family->id }}" method="post">
    {{ csrf_field() }}
    {{ method_field('PUT') }}


    <div class="form-group">
        <label>Código: </label>
        <input type="text" name="code" value="{{ $family->code }}">
    </div>

    <div class="form-group">
        <label>Nombre: </label>
        <input type="text" name="name" value="{{ $family->name }}">
    </div>

    <div class="form-group">
        <input type="submit" value="Guardar">
    </div>    
</form>
</div>
</div>
@endsection
