
@extends('layouts.app')

@section('content')
<div class="container">


<h1>
    Vista de familias
</h1>

<a href="/families/create">
Alta de familia
</a>

<table class="table">   

    <tr>
        <th>Id</th>
        <th>Código</th>
        <th>Nombre</th>
        <th></th>
    </tr>


@foreach ($families as $family)
    <tr>
        <td>{{ $family->id }}</td>
        <td>{{ $family->code }}</td>
        <td>{{ $family->name }}</td>
        <td>
        <a href="/families/{{ $family->id }}">Ver</a>
        <a href="/families/{{ $family->id }}/edit">Actualizar</a>

        <form action="/families/{{ $family->id }}" method="post">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
        <input type="submit" value="borrar">
        </form>


        </td>

    </tr>
@endforeach
</table>
{{ $families->links() }}
</div>
@endsection
