@extends('layouts.app')

@section('content')
<div class="container">


    <h1>
        Detalle de level
    </h1>

    <ul>
        <li>Codigo: {{ $level->code }}</li>
        <li>Nombre: {{ $level->name }}</li>
    </ul>

    <p><a href="/levels/{{ $level->id }}/edit">Modificar</a></p>

    <h3>Lista de estudios de este nivel</h3>
    <ul>
        @foreach ($level->studies as $study)
        <li>{{ $study->name }}</li>
        @endforeach
    </ul>
</div>
@endsection
