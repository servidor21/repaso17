<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public function studies()
    {
        return $this->belongsToMany('App\Study')->withPivot('course', 'year');

        //muchos estudiantes tienen muchos estudios (n:m), con atributos course y year en la relación
    }
}
