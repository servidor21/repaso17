<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Study;
use \App\Level;
use \App\Module;
use \App\Family;
use \App\Student;



class StudyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('auth')->only('index');
        //$this->middleware('auth')->except('index','show');
    }


    public function index()
    {
        // Index con todos los registros: all()
        //$studies = \App\Study::all();
        // $studies = Study::all();

        // Index paginado: paginate($size)
        $studies = Study::paginate(2);
        return view('study.index', ['studies' => $studies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Utilizamos una de las dos comprobaciones (¡¡¡las dos a la vez NO!!!)

        //Comprobación1 de la política StudyPolicy (en Policies)
        //con autorize: si no puede -->error 403
        $this->authorize('create', App\Study::class);


        //Comprobación2 de la política StudyPolicy (en Policies)
        //con user->can podemos redirigir donde sea
    //    if ($user->can('create', Study::class)) {
            //Executes the "create" method on the relevant policy...

    //    }else{
    //        redirect('xxxxx')
    //    }




        $levels = Level::all();
        $families = Family::all();
        return view('study.create', ['levels' => $levels, 'families' => $families ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255|min:2',
            'shortName' => 'required|max:255',
            'code' => 'required|unique:studies|max:6',
            'abreviation' => 'required'
        ]);



        $study = new Study($request->all());
        $study->save();
        $id = $study->id;
        return redirect('/studies/'.$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $study = Study::findOrFail($id);
        return view('study.show', ['study' => $study]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $study = Study::findOrFail($id);
        $levels = Level::all();
        return view('study.edit', [
            'study' => $study,
            'levels' => $levels,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // $this->validate($request, [
        //     'name' => 'required|max:255|min:2',
        //     'shortName' => 'required|max:255',
        //     'code' => 'required|unique:studies|max:6',
        //     'abreviation' => 'required',
        //     'level_id' => 'required',
        // ]);

        $study = Study::findOrFail($id);
        $study->code = $request->code;
        $study->name = $request->name;
        $study->shortName = $request->shortName;
        $study->abreviation = $request->abreviation;
        $study->level_id = $request->level_id;

        $study->save();
        return redirect('/studies/' . $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // solucion 1
        $study = Study::findOrFail($id);
        $this->authorize('delete', $study);
        $study->delete();

        // Solución 2
        
        //Study::destroy($id);
        return redirect('/studies');
    }

    public function modules($id)
    {
        $study = Study::with('modules')->findOrFail($id);
        // return $study;

        //devolver la lista de módulos        
        // return $study->modules;

        //devolver el estudio con el array de modulos
        return view('study.modules', ['study' => $study]);
        // return $study;
    }

    public function students($id)
    {
        $study = Study::with('students')->findOrFail($id);
        // return $study;

        //devolver la lista de módulos        
        // return $study->modules;

        //devolver el estudio con el array de modulos
        return view('study.students', ['study' => $study]);
        // return $study;
    }

    public function remember($id, Request $request)
    {
        $path = request()->headers->get('referer');
        $study = Study::findOrFail($id);
        //session(['lastStudy'=> $study]);
        \Session::put('lastStudy', $study);//meto elementos nuevos
        \Session::push('studies', $study);

        //Para que me reenvie a la pag actual
        $path = request()->headers->get('referer');
        return redirect($path);

    }

    public function list()
    {
        return view('study.list');
    }

    public function clearList()
    {
        //borrar el array
        //pero me da problemas el foreach
        //\Session::forget('studies');
        
        //para evitar error en foreach meto array vacio
        \Session::put('studies', array());
        return view('study.list');
    }
}
