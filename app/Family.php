<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Family extends Model
{
    public function studies()
    {
        return $this->hasMany('App\Study'); 

        //una familia tiene muchos estudios (1:n) 
    }
}
