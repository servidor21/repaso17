<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Study extends Model
{
    protected $fillable = ['code', 'name', 'shortName', 'abreviation', 'level_id', 'family_id'];

    public function level()
    {
        return $this->belongsTo('App\Level');

        //muchos estudios pertenecen a un nivel (n:1)
    }

    public function family()
    {
        return $this->belongsTo('App\Family');

        //muchos estudios pertenecen a una familia (n:1)
    }

    public function modules()
    {
        return $this->belongsToMany('App\Module')->withPivot('course');

        //muchos estudios tienen muchos módulos (n:m), con atributo course en la relación
    }

    public function students()
    {
        return $this->belongsToMany('App\Student')->withPivot('course', 'year');

        //muchos estudios tienen muchos estudiantes (n:m), con atributos course y year en la relación
    }
}
