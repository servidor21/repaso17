<?php

use Illuminate\Database\Seeder;

class ModuleStudyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('module_study')->insert([
            'module_id' => 1,
            'study_id' => 1,
            'course' => 1
            ]);

        DB::table('module_study')->insert([
            'module_id' => 2,
            'study_id' => 1,
            'course' => 1
            ]);

        DB::table('module_study')->insert([
            'module_id' => 3,
            'study_id' => 1,
            'course' => 1
            ]);

        DB::table('module_study')->insert([
            'module_id' => 1,
            'study_id' => 2,
            'course' => 1
            ]);

    }
}
