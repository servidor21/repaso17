<?php

use Illuminate\Database\Seeder;

class StudentStudyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('student_study')->insert([
            'student_id' => 1,
            'study_id' => 1,
            'course' => 1,
            'year' => 2017
            ]);

        DB::table('student_study')->insert([
            'student_id' => 2,
            'study_id' => 1,
            'course' => 1,
            'year' => 2016
            ]);

        DB::table('student_study')->insert([
            'student_id' => 3,
            'study_id' => 1,
            'course' => 1,
            'year' => 2017
            ]);

        DB::table('student_study')->insert([
            'student_id' => 1,
            'study_id' => 2,
            'course' => 1,
            'year' => 2015
            ]);
    }
}
