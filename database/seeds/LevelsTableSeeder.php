<?php

use Illuminate\Database\Seeder;

class LevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('levels')->insert([
            'id' => 1,
            'code' => 'ESO',
            'name' => 'Ed. Secundaria',
            ]);
        DB::table('levels')->insert([
            'id' => 2,
            'code' => 'BACH',
            'name' => 'Bachillerato',
            ]);
        DB::table('levels')->insert([
            'id' => 3,
            'code' => 'FPB',
            'name' => 'FP Básica',
            ]);
        DB::table('levels')->insert([
            'id' => 4,
            'code' => 'GM',
            'name' => 'C.F. Grado Medio',
            ]);
        DB::table('levels')->insert([
            'id' => 5,
            'code' => 'GS',
            'name' => 'C.F. Grado Superior',
            ]);
    }
}
