<?php

use Illuminate\Database\Seeder;

class StudiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('studies')->insert([
            'code' => 'IFC303',
            'name' => 'Grado Superior en Desarrollo de Aplicaciones Web',
            'shortName' => 'Desarrollo de Aplicaciones Web',
            'abreviation' => 'DAW',
            'level_id' => 5,
            'family_id' => 1
            ]);
        DB::table('studies')->insert([
            'code' => 'IFC302',
            'name' => 'Grado Superior en Desarrollo de Aplicaciones Multiplataforma',
            'shortName' => 'Desarrollo de Aplicaciones Multiplataforma',
            'abreviation' => 'DAM',
            'level_id' => 5,
            'family_id' => 1
            ]);
    }
}
