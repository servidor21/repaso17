<?php

use Illuminate\Database\Seeder;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('students')->insert([
            'dni' => '28123445Z',
            'firstname' => 'Roberto',
            'lastname' => 'Garcia',
            ]);
        DB::table('students')->insert([
            'dni' => '23126457P',
            'firstname' => 'Ivan',
            'lastname' => 'Campo',
            ]);
        DB::table('students')->insert([
            'dni' => '29123567W',
            'firstname' => 'Belen',
            'lastname' => 'Rueda',
            ]);
    }
}
